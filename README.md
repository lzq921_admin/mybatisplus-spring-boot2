# mybatisplus-spring-boot  Demo

Mybatis Plus - Spring Boot Demo 整合

参考：

[中文](http://mp.baomidou.com/) | [English](http://mp.baomidou.com/en/)

# 原理 | Principle

[Mybatis-Plus 实践及架构原理](http://git.oschina.net/baomidou/mybatis-plus/attach_files)

# 应用实例 | Demo

[Spring-MVC](https://git.oschina.net/baomidou/mybatisplus-spring-mvc)

[Spring-Boot](https://git.oschina.net/baomidou/mybatisplus-spring-boot)

[SSM-实战 Demo](https://gitee.com/baomidou/SpringWind)

# 下载地址 | Download

[点此去下载](http://maven.aliyun.com/nexus/#nexus-search;quick~mybatis-plus)

```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus</artifactId>
    <version>maven 官方最新版本为准</version>
</dependency>
```

# 其他开源项目 | Other Project

- [基于Cookie的SSO中间件 Kisso](http://git.oschina.net/baomidou/kisso)
- [Java快速开发框架 SpringWind](http://git.oschina.net/juapk/SpringWind)
